//5. Find the sum of all salaries based on country. ( Group it based on country and then find the sum ).


function findTotalSalaryByCountry(employees) {
    if(Array.isArray(employees)){
        const totalSalaryByCountry = {};
        for (let val of employees){
            if (!totalSalaryByCountry[val.location]) {
                totalSalaryByCountry[val.location] = 0;
            }
            totalSalaryByCountry[val.location] += val.corrected_salary;
        }
        return totalSalaryByCountry;
    }
    else{
        return [""];
    }
}
module.exports = findTotalSalaryByCountry;