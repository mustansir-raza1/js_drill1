//  1. Find all Web Developers. ( It could be Web Developer III or Web Developer II or anything else )

function findWebDevelopers(employees) {
    if(Array.isArray(employees)){
        console.log("web developers: ")
        const webDevelopers = [];
        for (let i = 0; i < employees.length; i++) {
            if (employees[i].job.includes("Web Developer")) {
                webDevelopers.push(employees[i]);
            }
        }
        console.log (webDevelopers);
    }
    else{
        return [""];
    }
}
module.exports = findWebDevelopers;