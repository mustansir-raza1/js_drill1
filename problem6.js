// 6. Find the average salary of based on country. ( Groupd it based on country and then find the average ).

function averageSalaryByCountry(employees){
    if(Array.isArray(employees)){
        const totalSalaryByCountry = {};
        const countByCountry = {};
        for (let val of employees){
            if (!totalSalaryByCountry[val.location]) {
                totalSalaryByCountry[val.location] = 0;
                countByCountry[val.location]=0;
            }
            totalSalaryByCountry[val.location] += val.corrected_salary;
            countByCountry[val.location] += 1;
        }
        let averageSalary = {};
        for(let country in totalSalaryByCountry){
            averageSalary[country] = totalSalaryByCountry[country]/countByCountry[country];
        }
        return averageSalary;
    }
    else{
        return [""];
    }
}
module.exports = averageSalaryByCountry;