// 3. Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something

function salaryAmountFactor(employee){
    if(Array.isArray(employee)){
        for(let val of employee){
            let correctedSalary = val.salary * 10000;
            val.corrected_salary = correctedSalary
        }
        return employee;
    }
    else{
        return [];
    }
}
module.exports = salaryAmountFactor;