// 2. Convert all the salary values into proper numbers instead of strings.
function convertSalary(employees){
    if(Array.isArray(employees)){
        for(let val of employees){
            let salaryInString = val.salary;
            let salaryInNum = Number(salaryInString.replace("$", ""));
            val.salary = salaryInNum;
         }
         return employees;
    }
    else{
        return [""];
    }
    
}
module.exports = convertSalary;