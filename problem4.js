// 4. Find the sum of all salaries.

function sumOfSalaries(data){
    if(Array.isArray(data)){
        let sumOfSal = 0;
        for(let val of data){
            sumOfSal += val.corrected_salary;
        }
        return sumOfSal;
    }
    else{
        return [];
    }
    
}

module.exports = sumOfSalaries;